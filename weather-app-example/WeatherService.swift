//
//  WeatherService.swift
//  weather-app-example
//
//  Created by Ali Ebrahimi on 6/29/16.
//  Copyright © 2016 Ali Ebrahimi. All rights reserved.
//

import Foundation

protocol WeatherServiceDelegate {
    func setWeather( weather: Weather )
}

class WeatherService {
    
    var delegate: WeatherServiceDelegate?
    
    func getWeather(city : String) {
        
        let cityEscaped = city.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())
        
        let path = "http://api.openweathermap.org/data/2.5/weather?q=\(cityEscaped!)&appid=2fd486d0d63b373b62c0db5ff6547831"
        let url = NSURL(string: path)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url!) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            //print(">>> \(data)")
            
            let json = JSON(data: data!)
            let lon = json["coord"]["lon"].double
            let lat = json["coord"]["lat"].double
            let temp = json["main"]["temp"].double
            let name = json["name"].string
            let desc = json["weather"][0]["description"].string
            
            let weather = Weather(cityName: name!, temp: temp!, description: desc!)
            
            if self.delegate != nil{
                
                dispatch_async(dispatch_get_main_queue(), { 
                    self.delegate?.setWeather(weather)
                })
            }
            
            print("lat: \(lat!) lon: \(lon!) temp: \(temp!)")
        }
        
        task.resume()
        
        /*
        print("Weather ervice city : \(city)")
        
        // req weather data
        // wait ...
        // proccess data
        
        let weather = Weather(cityName: city, temp: 237.7, description: "y ruze khub")
        
        if delegate != nil{
            delegate?.setWeather(weather)
 
        }
 */
        
    }
}