//
//  Weather.swift
//  weather-app-example
//
//  Created by Ali Ebrahimi on 6/29/16.
//  Copyright © 2016 Ali Ebrahimi. All rights reserved.
//

import Foundation


struct Weather {
    let cityName: String
    let temp: Double
    let description : String
    
    init(cityName: String, temp: Double, description : String) {
        self.cityName = cityName
        self.temp = temp
        self.description = description
    }
}