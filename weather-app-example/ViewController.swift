//
//  ViewController.swift
//  weather-app-example
//
//  Created by Ali Ebrahimi on 6/29/16.
//  Copyright © 2016 Ali Ebrahimi. All rights reserved.
//

import UIKit

class ViewController: UIViewController , WeatherServiceDelegate{

    
    let weatherService = WeatherService()
    
    
    
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBAction func setCityButtom(sender: UIButton) {
        print("click buttom")
        openCityAlert()
    }
    
    
    func openCityAlert(){
        let alert = UIAlertController(title: "C I T Y",
                                      message: "Enter city name :",
                                      preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancel = UIAlertAction(title: "Cancel",
                                   style: UIAlertActionStyle.Cancel,
                                   handler: nil)
        
        alert.addAction(cancel)
        
        let ok = UIAlertAction(title: "Ok",
                               style: UIAlertActionStyle.Default) { (action : UIAlertAction) -> Void in
                                print("OK!")
                                let textField = alert.textFields?[0]
                                print(textField?.text!)
                                self.cityLabel.text = textField?.text!
                                let cityName = textField?.text
                                self.weatherService.getWeather(cityName!)
        }
        
        alert.addAction(ok)
        
        alert.addTextFieldWithConfigurationHandler { (textField : UITextField) -> Void in
            textField.placeholder = " City name "
        }
        
        self.presentViewController(alert ,
                                   animated: true,
                                   completion: nil)
    }
    
    
    // Mark - Weather Service Delegate
    
    func setWeather(weather: Weather) {
       // print("*** set weather")
       // print("city : \(weather.cityName) temp : \(weather.temp) desc : \(weather.description)")
        cityLabel.text = weather.cityName
        tempLabel.text = "\(weather.temp)"
        descLabel.text = weather.description
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.weatherService.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

